/* eslint-disable no-console */
// import './loader-externs'
import unfetch from 'unfetch'
import { loadStyle } from '@lemuria/load-scripts'
import { connect } from '@idio2/websocket'

// can be used to send update events to the server.
const connection = connect(listener, {
  channel: 'dev-server',
  reconnect: false,
})

// this whole ajax thing needs rework to replace whole page with head, etc.
async function listener(event, message) {
  switch (event) {
  case 'page-update': {
    // todo remount components
    const { 'pages': pages, 'mount': mount } = message
    const p = pages.find(({ 'u': u }) => {
      return u == decodeURIComponent(location.pathname)
    })
    if (!p) return
    const AJAX = `${mount}${mount.endsWith('/') ? '' : '/'}ajax/${p.url}`
    const res = await unfetch(AJAX, null)
    if (!res.ok) {
      console.error(`Could not fetch ${AJAX}`)
      return
    }
    const { title, content, postAjax } = await res.json()
    document.title = title
    document.querySelector('#Content').innerHTML = content
    if (postAjax) eval(postAjax)
    break
  }
  case 'block-update': {
    const parent = document.getElementById(message['id'])
    parent.outerHTML = message['html']
    break
  }
  case 'asset-update': {
    if (message.endsWith('.css')) {
      const el = document.querySelector(`[href="${message}"]`) || document.querySelector(`[data-href="${message}"]`)
      if (el) {
        const href = bustCache(message)
        loadStyle(href, (err, ev) => {
          if (err) {
            console.log('Could not load a reloaded style at %s', message)
            return
          }
          el.setAttribute('href', href)
          el.setAttribute('data-href', message)
          const newEl = ev.currentTarget
          setTimeout(() => {
            newEl.remove()
          }, 250)
          console.log('💃 Reloaded style %s', message)
        })
      } else console.warn('Received updated for %s asset, but a link with this href attribute was not found.', message)
    }
    break
  }
  default:
    console.log('Dev Server Received:', message)
  }
}

const bustCache = (s) => {
  return `${s}?t=${new Date().getTime()}`
}