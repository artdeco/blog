import { Component } from 'preact'

export default class ShowHideMenuButton extends Component {
  constructor() {
    super()
    this.state = {
      hiding: false,
    }
  }
  render({ onShow, onHide, test, splendid, ...props }) {
    const { hiding } = this.state
    return (<a {...props} href="#" onClick={(ev) => {
      ev.preventDefault()
      const newHiding = !hiding
      this.setState({
        hiding: newHiding,
      })
      if (newHiding && onShow) onShow(ev)
      if (!newHiding && onHide) onHide(ev)
      return false
    }}>
      {hiding ? 'show' : 'hide'} menu {test}
    </a>)
  }
}