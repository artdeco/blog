import { Component } from 'preact'
import ShowHideMenuButton from './SideBarButton'

export default class SideBarComponent extends Component {
  render({ $sidebarshowing, ...props }) {
    return (<ShowHideMenuButton {...props} onHide={(e) => {
      const target = /** @type {!Element} */ (e.target)
      target.parentElement.classList.add($sidebarshowing)
    }} onShow={(e) => {
      const target = /** @type {!Element} */ (e.target)
      target.parentElement.classList.remove($sidebarshowing)
    }} />)
  }
}