## 25 February 2021

- [Fix] Fix the RUDA font on the body which became overridden by bootstrap before after layout change.
- [Blog Post] Talk about `names.co.uk` security vulnerability in feb 24 post.

## 23 February 2021

### 0.1.3

- Enable on-the-page scrolling for inner headings
- [Blog Post] Finish the post by implementing the sidemenu component.

### 0.1.2

- [Elements] Refactor the `sidemenu` element to have a component instead of the init function.
- [Blog Post] Write about refactoring the sidemenu.

### 0.1.1

- [Blog Post] 20 Sat for book expressions.
- [Refactor] Re-enable the ellipsis widget.

## 23 February 2020

### 0.0.0

- [package] Create a new website with [Splendid](https://www.npmjs.com/package/splendid).