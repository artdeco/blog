const CACHE_NAME = 'cache-01'

self.addEventListener('install', async (event) => {
  event.waitUntil((async () => {
    debugger
    await caches.open(CACHE_NAME)
    const cache = await caches.open(CACHE_NAME)
    await cache.addAll(
      [
        // '/blog/img/st-319.jpg',
        // '/blog/img/st-319.webp',
        // '/css/bootstrap.css',
        // '/css/main.css',
        // '/js/bootstrap.min.js',
        // '/js/jquery.min.js',
        // '/offline.html',
      ],
    )
  })())
})