/** @type {import('splendid').Page} */
const mar = {
  title: 'March',
  seo: 'Spring is here.',
  og: {
    image: '/img/logo.jpg',
  },
  reverse: true,
}

/** @type {import('splendid').Page} */
const feb = {
  title: 'February',
  seo: 'Starting to write a new blog about work.',
  og: {
    image: '/img/logo.jpg',
  },
  reverse: true,
}

/** @type {import('splendid').Page} */
const index = {
  seo: 'Art Deco blog for 2020.',
  og: {
    image: '/img/logo.jpg',
  },
  reverse: true,
}

const _2020 = {
  mar, feb, index,
}

for (const key in _2020) {
  _2020[key].year = 2020
}

export default _2020