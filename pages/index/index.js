/** @type {import('splendid').Page} */
const index = {
  aliases: ['index'],
  seo: '1 year later.',
  url: '/index.html',
  menuUrl: '/',
  file: '.',
  og: {
    image: '/img/logo.jpg',
  },
  reverse: true,
}

const june2021 = {
  index,
  'tue_15': {
    // focus: true,
    file: '15-tue',
    post: true,
    reverse: true,
  },
  'wed_16': {
    // focus: true,
    file: '16-wed',
    post: true,
    reverse: true,
  },
}

for (const key in june2021) {
  june2021[key].year = 2021
}

export default june2021