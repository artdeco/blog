/** @type {import('splendid').Page} */
const index = {
  // aliases: ['index'],
  seo: 'Art Deco Blog February.',
  // url: '/index.html',
  // menuUrl: '/',
  file: '.',
  og: {
    image: '/img/logo.jpg',
  },
  reverse: true,
}

const february2021 = {
  index,
  'sat_27': {
    file: '27-sat',
    post: true,
    reverse: true,
  },
  'fri_26': {
    file: '26-fri',
    post: true,
    reverse: true,
  },
  'thu_25': {
    file: '25-thu',
    post: true,
    reverse: true,
  },
  'wed_24': {
    file: '24-wed',
    post: true,
    reverse: true,
  },
  'tue_23': {
    file: '23-tue',
    post: true,
    reverse: true,
  },
  'sat_20': {
    file: '20-sat',
    post: true,
    reverse: true,
  },
}

for (const key in february2021) {
  february2021[key].year = 2021
}

export default february2021