const CLASS = 'SideBarMenu'

/**
 * A sticky sidebar.
 * @param {Object} p
 * @param {import('splendid')} p.splendid
 */
export default function Sidebar({
  children, id='sidebar', splendid, class: c, ...props
}) {
  const cssPath = splendid.elementRelative('./sidebar.css')
  const {
    $SideBarContent, $HideMenu, $ShowMenu, $sidebarshowing,
  } = splendid.css(cssPath, `.${CLASS}`)
  const cn = [c, CLASS, $sidebarshowing].filter(Boolean).join(' ')
  return (<div data-showing-sel={$sidebarshowing} data-hide-sel={$HideMenu}
    data-show-sel={$ShowMenu} {...props} sticky-top className={cn} id={id}>
    <div className={$SideBarContent}
      dangerouslySetInnerHTML={{ __html: children }}/>

    <a className={$HideMenu} href="#">hide menu</a>
    <a className={$ShowMenu} href="#">show menu</a>
  </div>)
}

export const init = ({ id }) => {
  /* eslint-env browser */
  const el = document.getElementById(id)
  const hs = el.dataset['hideSel']
  delete el.dataset['hideSel']
  const ss = el.dataset['showSel']
  delete el.dataset['showSel']
  const sw = el.dataset['showingSel']
  delete el.dataset['showingSel']
  const hm = el.querySelector(`.${hs}`)
  const sm = el.querySelector(`.${ss}`)
  if (hm) {
    hm.onclick = (e) => {
      const target = /** @type {!Element} */ (e.target)
      target.parentElement.classList.remove(sw)
      return false
    }
  }
  if (sm) {
    sm.onclick = (e) => {
      const target = /** @type {!Element} */ (e.target)
      target.parentElement.classList.add(sw)
      return false
    }
  }
}
