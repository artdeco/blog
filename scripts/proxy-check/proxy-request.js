import aqt from '@rqt/aqt/src'
import loading from 'indicatrix'
import { PROXIES } from '../names/proxie'
import { parse } from 'url'
import { selectRandom } from './lib'

export async function proxyRequest(url, loadingText, opts = {}) {
  const {
    proxies = PROXIES,
    UA = 'Mozilla/5.0 (Windows Phone 10.0; Android 4.2.1; Microsoft; Lumia 950) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Mobile Safari/537.36 Edge/13.1058',
    reqOpts,
    log = () => {},
    timeout = 10000,
    disable = false,
  } = opts
  const [PROXY, PROXY_PORT, HTTPS = false] = selectRandom(proxies)
  log('Making proxy request to %s via %s:%s', url, PROXY, PROXY_PORT)
  const newPath = {
    hostname: PROXY.trim(), protocol: HTTPS ? 'https:' : 'http:', port: PROXY_PORT, path: url,
  }
  const res2 = await loading(loadingText, aqt(newPath, {
    headers: {
      'User-Agent': UA,
    },
    timeout,
    ...reqOpts,
  }))
  return res2
}