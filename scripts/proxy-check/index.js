import { proxyRequest } from './proxy-request'
import { PROXIES } from '../names/proxie'
import { join } from 'path'
import bosom from 'bosom'
import { equal } from 'assert'

const GOOD = join(__dirname, 'good.json')

export default async function proxyCheck(proxies) {
  let good = {}
  const newGood = {}
  try {
    good = await bosom(GOOD)
  } catch (err) {
    // ok
  }
  for (const proxy of proxies) {
    // console.log(proxy)
    const p = `${proxy[0]}:${proxy[1]}`
    const d = new Date().getTime()
    let res = false, errCode
    try {
      const tester = 'https://london.type.engineering/ip'
      res = await proxyRequest(tester, `${tester} via ${proxy[0]}`, {
        proxies: [proxy],
        timeout: 20000,
      })
      const { body } = res
      if (body != proxy[0]) {
        console.log('Expected %s, got %s', proxy[0], body)
      }
      equal(body, proxy[0])
    } catch (err) {
      if (err.code) errCode = err.code
    }
    const dd = new Date().getTime() - d
    if (errCode) console.log('[-]', p, errCode, `${dd}ms`)
    else if (res) {
      newGood[p] = dd
      console.log('[+]', p, `${dd}ms`)
    }
  }
  if (Object.keys(newGood).length) {
    const all = { ...good, ...newGood }
    console.log(all)
    await bosom(GOOD, all, {
      space: 1,
    })
  }
}

(async () => {
  await proxyCheck(PROXIES)
})()