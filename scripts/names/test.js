import CookieContext from '@contexts/http/cookies'

export const context = CookieContext

/** @type {Object<string, (c:CookieContext)} */
export const Obj = {
  async 'returns undefined without cookie'({ start, c }) {
    await start(c((req, res, cookies) => {
      res.end(String(cookies.get('fizz')))
    }))
      .set('Cookie', 'foo=bar')
      .get('/')
      .assert(200, 'undefined')
  },
}