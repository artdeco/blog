export const COOKIE = [
  ['cookiePrefs', '%7B%22updated%22%3Afalse%2C%22categories%22%3A%7B%22analytics%22%3Afalse%7D%7D'],
  ['cookid', 'a4c8eec970a254fcb7c839bf855b4dfa'],
  ['dbmFP', 'a4c8eec970a254fcb7c839bf855b4dfa.EG0_Tf'],
  ['dbmPK', 'a4c8eec970a254fcb7c839bf855b4dfa.EG0_Tf'],
  ['xsc', '3bc5859140621a6039fab91444ae40b8865630b35eebb392812adb64caa8a812ea9fec397bf360da0cab7aa05e15d7beef3c976e94c86a9b44821916de74ed40'],
  ['_wicasa', 'JSON_eyJjaWQiOiJhNGM4ZWVjOTcwYTI1NGZjYjdjODM5YmY4NTViNGRmYSIsImNrdGltZSI6IjE2MTQxNjQyMzUiLCJzY3AiOiIzMCIsInBrIjoiRUcwX1RmIiwiY2tib3JuIjoiMTYxNDE2MDE3MiIsImxhc3RyZWYiOiJ3d3cubmFtZXMuY28udWtcL2NhcnRcL2luZGV4XC90aGFua3lvdSIsImxhc3Rkb21zcmMiOiJyZWZsZWN0aW9uc2JlYXV0eS5pZWZhbHNlIiwiaXNjbGkiOiIxIn0%3D'],
  ['_ashkii', 'JSON_eyJzaWQiOiJhNGM4ZWVjOTcwYTI1NGZjYjdjODM5YmY4NTViNGRmYSIsImNrdGltZSI6IjE2MTQxNjQyMzUiLCJzc3AiOiIzMCIsInBwIjoiRUcwX1RmIn0%3D'],
].reduce((acc, [key, val]) => {
  return acc + `${key}=${val}; `
}, '').trim()