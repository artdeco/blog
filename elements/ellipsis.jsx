import Render from '../components/ellipsis/render'

export default function serverRender({ splendid, timeout, children }) {
  splendid.export()
  // when need to limit props that reach the browser,
  // splendid.export({ timeout }) can be used.

  return Render({ timeout, children })
  // just return HTML for placeholder until component
  // is initialised on the page.
}