
// let classes be imported by alamode
// hopes and dreams
// import { $SideBarContent, $HideMenu, $ShowMenu, $sidebarshowing } from './sidebar.css'
// eg
// const cssPath = splendid.elementRelative('./sidebar.css')

import ShowHideMenuButton from '../../components/sidebar/SideBarButton'

// const { $SideBarContent, $HideMenu, $ShowMenu, $sidebarshowing } = splendid.css(cssPath, `.${CLASS}`)
const CLASS = 'SideBarMenu'

/**
 * A sticky sidebar.
 * @param {Object} p
 * @param {import('splendid')} p.splendid
 */
export default function Sidebar({
  children, splendid, class: c, ...props
}) {
  const cssPath = splendid.elementRelative('./sidebar.css')
  const buttonId = 'sidebar-btn'
  const { $SideBarContent, $MenuButton, $sidebarshowing } = splendid.css(cssPath, `.${CLASS}`)
  const cn = [c, CLASS, $sidebarshowing].filter(Boolean).join(' ')
  splendid.export({ $sidebarshowing, className: $MenuButton }, [], { id: buttonId })
  const ind = splendid.getChildIndentLeft2(children)
  return (<div {...props} sticky-top className={cn}>
    <div className={$SideBarContent} dangerouslySetInnerHTML={{ __html: ind }}/>
    <ShowHideMenuButton className={$MenuButton} id={buttonId} />
  </div>)
}