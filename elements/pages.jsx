/**
 * @param {Object} props
 * @param {Splendid} props.splendid
 * @param {string} [dir] Pages from this directory only.
 * @param {boolean} [indexes] Print links that are indexes of dirs only.
 * @param {boolean} [same-dir] Only include pages from the same dir.
 */
export default function Pages({ splendid, dir, indexes, 'same-dir': sameDir }) {
  const { pages, page: { key, dir: currentPageDir } } = splendid
  const menuPages = pages.filter(({ dir: d, index }) => {
    if (indexes) {
      if (dir) {
        if (`${d}/`.startsWith(dir)) {
          // nothing
        } else if (d == dir) {
          // nothing
        }
        else return false
      }
      return d && index
    }
    if (sameDir) {
      return currentPageDir == d
    }
    if (dir) return d == dir
    if (!d) return true
  })
  const ajax = (<ul>
    {menuPages.map(({
      title, menu = title, url, menuUrl = url, file, key: k, dir: d,
    }) => {
      const active = k == key || (currentPageDir == d)
      return (<li className={active ? 'Active' : ''}>
        <a data-file={file} href={k}>{menu}</a>
      </li>)
    }
    )}
  </ul>)
  return ajax
}

/**
 * @typedef {import('splendid')} Splendid
 */