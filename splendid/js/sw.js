/* eslint-env browser */
if ('serviceWorker' in navigator) {
  window.addEventListener('load', async () => {
    try {
      const registration = await navigator.serviceWorker.register('/blog/service-worker.js')
      console.log('Service Worker is registered', registration)
    } catch (err) {
      console.error('Registration failed:', err)
    }
  })
}