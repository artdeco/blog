import __initOnThisPage from '../__init/on-this-page'
import '../../js/sw.js'
import __renameMap0 from '../__rename-maps/../elements/sidebar/sidebar'
import __renameMap1 from '../__rename-maps/../elements/on-this-page/on-this-page'
import makeClassGetter from '../__mcg'
const renameMaps = { '../elements/sidebar/sidebar.css': __renameMap0,
  '../elements/on-this-page/on-this-page.css': __renameMap1 }
__initOnThisPage()
import { Component, render, h } from 'preact'
import { makeIo, init, start } from '../__competent-lib'
import Ellipsis from '../../../components/ellipsis/index.jsx'
import GifVideo from '../../../components/gif-video.js'
import Highlightjs from 'splendid/build/components/highlightjs/index'
import Sidebar from '../../../components/sidebar/index.jsx'
import SocialButtons from 'splendid/build/components/social-buttons'

const __components = {
  'ellipsis': () => Ellipsis,
  'gif-video': () => GifVideo,
  'highlightjs': () => Highlightjs,
  'sidebar': () => Sidebar,
  'social-buttons': () => SocialButtons,
}

const io = makeIo()

/** @type {!Array<!preact.PreactProps>} */
const meta = [{
  key: 'social-buttons',
  id: 'c3952',
  props: {
    url: 'https://artdeco.gitlab.io/blog/year2020/march.html',
    meta: true,
    className: 'b-xq b-Hk',
    style: '',
  },
},
{
  key: 'highlightjs',
  id: 'c639c',
  props: {
    lang: 'shell',
  },
},
{
  key: 'highlightjs',
  id: 'cdd8f',
  props: {
    lang: 'markdown',
  },
},
{
  key: 'sidebar',
  id: 'sidebar-btn',
  props: {
    $sidebarshowing: 'c',
    className: 'a',
  },
},
{
  key: 'highlightjs',
  id: 'cd232,cd2321,cd2322,cd2323',
  props: {
    lang: 'json',
  },
},
{
  key: 'highlightjs',
  id: 'c9b54,c9b541,c9b5410,c9b542,c9b543,c9b544,c9b545,c9b546,c9b547,c9b548,c9b549',
  props: {
    lang: 'javascript',
  },
},
{
  key: 'ellipsis',
  id: 'ceb55',
  props: {
    timeout: 300,
  },
  children: ["\n  Please bear one moment while I add the content\n"],
},
{
  key: 'highlightjs',
  id: 'c138c',
  props: {
    lang: 'css',
  },
},
{
  key: 'highlightjs',
  id: 'c67ef',
},
{
  key: 'ellipsis',
  id: 'c06ef',
},
{
  key: 'gif-video',
  id: 'c5c0e',
  props: {
    src: '../pages/2020/mar/img/fire2.mp4',
  },
}]
meta.forEach(({ key, id, props = {}, children = [] }) => {
  const Comp = __components[key]()
  const plain = Comp.plain || (/^\s*class\s+/.test(Comp.toString()) && !Component.isPrototypeOf(Comp))
  props.splendid = { mount: '/blog/', addCSS(stylesheet) {
    return makeClassGetter(renameMaps[stylesheet])
  }, css(paths, rootSelector, { mapName } = {}) {
    if (mapName && !mapName.endsWith('.js')) {
      mapName = mapName += '.js'
    }
    return this.addCSS(mapName || paths)
  },
  elementRelative(path) { return `${path}` },
  export() {},
}

  const ids = id.split(',')
  ids.forEach((Id) => {
    const { parent, el } = init(Id, key)
    if (!el) return
    const renderMeta = /** @type {_competent.RenderMeta} */ ({ key, id: Id, plain })
    let comp
    el.rerender = () => {
      if (!comp) return
      const getComp = () => __components[key]()
      comp.rerender(getComp, { children, ...props })
    }
    el.render = () => {
      comp = start(renderMeta, Comp, comp, el, parent, props, children, { render, Component, h })
      return comp
    }
    el.render.meta = renderMeta
    io.observe(el)
  })
})
