import __initOnThisPage from '../../__init/on-this-page'
import '../../../js/sw.js'
import __renameMap0 from '../../__rename-maps/../elements/sidebar/sidebar'
import __renameMap1 from '../../__rename-maps/../elements/on-this-page/on-this-page'
import makeClassGetter from '../../__mcg'
const renameMaps = { '../elements/sidebar/sidebar.css': __renameMap0,
  '../elements/on-this-page/on-this-page.css': __renameMap1 }
__initOnThisPage()
import { Component, render, h } from 'preact'
import { makeIo, init, start } from '../../__competent-lib'
import GifVideo from '../../../../components/gif-video.js'
import Highlightjs from 'splendid/build/components/highlightjs/index'
import Sidebar from '../../../../components/sidebar/index.jsx'
import SocialButtons from 'splendid/build/components/social-buttons'

const __components = {
  'gif-video': () => GifVideo,
  'highlightjs': () => Highlightjs,
  'sidebar': () => Sidebar,
  'social-buttons': () => SocialButtons,
}

const io = makeIo()

/** @type {!Array<!preact.PreactProps>} */
const meta = [{
  key: 'gif-video',
  id: 'c9511',
  props: {
    src: '../../pages/2021/feb/24-wed/img/bruteforce.mp4',
  },
},
{
  key: 'sidebar',
  id: 'sidebar-btn',
  props: {
    $sidebarshowing: 'c',
    className: 'a',
  },
},
{
  key: 'social-buttons',
  id: 'cb2d8',
  props: {
    url: 'https://artdeco.gitlab.io/blog/year2021/february/24-wed-namescouk-adventures.html',
    meta: true,
    className: 'b-xq b-Hk',
    style: '',
  },
},
{
  key: 'highlightjs',
  id: 'c9b54,c9b541',
  props: {
    lang: 'javascript',
  },
},
{
  key: 'highlightjs',
  id: 'c67ef,c67ef1',
}]
meta.forEach(({ key, id, props = {}, children = [] }) => {
  const Comp = __components[key]()
  const plain = Comp.plain || (/^\s*class\s+/.test(Comp.toString()) && !Component.isPrototypeOf(Comp))
  props.splendid = { mount: '/blog/', addCSS(stylesheet) {
    return makeClassGetter(renameMaps[stylesheet])
  }, css(paths, rootSelector, { mapName } = {}) {
    if (mapName && !mapName.endsWith('.js')) {
      mapName = mapName += '.js'
    }
    return this.addCSS(mapName || paths)
  },
  elementRelative(path) { return `${path}` },
  export() {},
}

  const ids = id.split(',')
  ids.forEach((Id) => {
    const { parent, el } = init(Id, key)
    if (!el) return
    const renderMeta = /** @type {_competent.RenderMeta} */ ({ key, id: Id, plain })
    let comp
    el.rerender = () => {
      if (!comp) return
      const getComp = () => __components[key]()
      comp.rerender(getComp, { children, ...props })
    }
    el.render = () => {
      comp = start(renderMeta, Comp, comp, el, parent, props, children, { render, Component, h })
      return comp
    }
    el.render.meta = renderMeta
    io.observe(el)
  })
})
