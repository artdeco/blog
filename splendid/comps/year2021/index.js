import __initOnThisPage from '../__init/on-this-page'
import '../../js/sw.js'
import __renameMap0 from '../__rename-maps/../elements/sidebar/sidebar'
import __renameMap1 from '../__rename-maps/../elements/on-this-page/on-this-page'
import makeClassGetter from '../__mcg'
const renameMaps = { '../elements/sidebar/sidebar.css': __renameMap0,
  '../elements/on-this-page/on-this-page.css': __renameMap1 }
__initOnThisPage()
import { Component, render, h } from 'preact'
import { makeIo, init, start } from '../__competent-lib'
import Sidebar from '../../../components/sidebar/index.jsx'
import SocialButtons from 'splendid/build/components/social-buttons'

const __components = {
  'sidebar': () => Sidebar,
  'social-buttons': () => SocialButtons,
}

const io = makeIo()

/** @type {!Array<!preact.PreactProps>} */
const meta = [{
  key: 'social-buttons',
  id: 'c83af',
  props: {
    url: 'https://artdeco.gitlab.io/blog/year2021/',
    meta: true,
    className: 'b-xq b-Hk',
    style: '',
  },
},
{
  key: 'sidebar',
  id: 'sidebar-btn',
  props: {
    $sidebarshowing: 'c',
    className: 'a',
  },
}]
meta.forEach(({ key, id, props = {}, children = [] }) => {
  const Comp = __components[key]()
  const plain = Comp.plain || (/^\s*class\s+/.test(Comp.toString()) && !Component.isPrototypeOf(Comp))
  props.splendid = { mount: '/blog/', addCSS(stylesheet) {
    return makeClassGetter(renameMaps[stylesheet])
  }, css(paths, rootSelector, { mapName } = {}) {
    if (mapName && !mapName.endsWith('.js')) {
      mapName = mapName += '.js'
    }
    return this.addCSS(mapName || paths)
  },
  elementRelative(path) { return `${path}` },
  export() {},
}

  const ids = id.split(',')
  ids.forEach((Id) => {
    const { parent, el } = init(Id, key)
    if (!el) return
    const renderMeta = /** @type {_competent.RenderMeta} */ ({ key, id: Id, plain })
    let comp
    el.rerender = () => {
      if (!comp) return
      const getComp = () => __components[key]()
      comp.rerender(getComp, { children, ...props })
    }
    el.render = () => {
      comp = start(renderMeta, Comp, comp, el, parent, props, children, { render, Component, h })
      return comp
    }
    el.render.meta = renderMeta
    io.observe(el)
  })
})
