export default ({ id }) => {
  /* eslint-env browser */
  const el = document.getElementById(id)
  const hs = el.dataset['hideSel']
  delete el.dataset['hideSel']
  const ss = el.dataset['showSel']
  delete el.dataset['showSel']
  const sw = el.dataset['showingSel']
  delete el.dataset['showingSel']
  const hm = el.querySelector(`.${hs}`)
  const sm = el.querySelector(`.${ss}`)
  if (hm) {
    hm.onclick = (e) => {
      const target = /** @type {!Element} */ (e.target)
      target.parentElement.classList.remove(sw)
      return false
    }
  }
  if (sm) {
    sm.onclick = (e) => {
      const target = /** @type {!Element} */ (e.target)
      target.parentElement.classList.add(sw)
      return false
    }
  }
}