/** @type {import('splendid').Config} */
const config = {
  layout: 'splendid/layout/main.html',
  replacements: [
    {
      re: /{{ company }}/g,
      replacement: '[Art Deco™](https://www.artd.eco)',
    },
    {
      re: /{{ page\.year }}/g,
      replacement() {
        return this.splendid.page.year
      },
    },
    {
      re: / (Keybase|GitLab(?: Pages)?|GitHub(?: Actions)?|Idio|Splendid|StackOverflow|Preact|React|Bulma|Bootstrap)([ ,.'])/g,
      replacement: ' _$1_$2',
    },
    {
      re: /^(Keybase|GitLab(?: Pages)?|GitHub( Actions)|GitHub|Idio|Splendid|StackOverflow|Preact|React|Bulma|Bootstrap) /gm,
      replacement: '_$1_ ',
    },
  ],
  pages: '../pages',
  elements: ['../elements'],
  components: ['../components'],
  blocks: ['../blocks'],
  // which prefixes to keep in the main CSS
  prefixes: ['-webkit-hyphens', '-ms-hyphens'],
  // for sitemap and social-buttons
  url: 'https://artdeco.gitlab.io/blog/',
  // required when pages are at org.github.io/pages-name
  mount: '/blog/',
  potracePath: '~/.splendid/potrace',
}

export default config